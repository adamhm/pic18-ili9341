/*
 *
 *	PIC18-ILI9341 demo
 *	This is a basic program to demonstrate usage of the PIC18-ILI9341 library functions.
 *
 *	This is intended for the PIC18F2xK42 series microcontrollers - the default is a PIC18F24K42 but if you're using one of the others
 *	in this range then simply change the device selection for the specific one you are using and switch the header files accordingly
 *	(initial testing was on a PIC18F27K42)
 *
 *	The LCD module should be connected to pins RC3-7:
 *	CS = RC3
 *	Reset = RC4
 *	DC/RS = RC5
 *	SDO = RC6
 *	SCK = RC7
 *	SDI is unused
 *
 *	There are a few things that can be done to help speed things up a little, depending on your project:
 *
 *	- If the display is the only SPI device connected you can connect a pulldown to the display's CS pin and keep it permanently active, removing the need to toggle
 *		the CS pin.
 *	- If you don't need to read from the SPI port you can disable RX mode and remove the lines to clear the RX buffer after each transmit
 *	- You could front-load the SPI status checks to allow the rest of the program to continue while data is being transmitted, minimising delays between writes
 *	- The PIC18xxK42 range features 2-byte FIFO buffers which is convenient as the ILI9341 requires a lot of 2-byte writes, so with these devices you can remove the
 *		wait for TX to complete between bytes and just write both bytes to the TX buffer without any delay.
 *
 *	Backlight control:
 *
 *	The display I used is a 2.2" module with the SKU 'MSP2202' and there appears to be two versions of it. It's fine to just hook power up directly to the LED pin for
 *	both versions but if you want to control the brightness/reduce power draw then the method required will differ.
 *
 *	The first version has a 5-pin header for the SD card socket and uses a simple transistor switch for the backlight, so controlling its brightness involves using PWM.
 *
 *	The newer version of the module (marked "v1.1") has a 4-pin header for the SD card socket and the backlight LED pin is connected directly to the backlight LEDs via a resistor.
 *	Controlling the brightness involves adding another resistor in series with it or using a P-type transistor & inverted PWM to control it.
 *
 *	There are other differences between the two modules as well: the positioning of the panel and pins are slightly different and the panel used on the newer version of the module
 *	seems to be inferior to the older version.
 *
 */

// PIC18F24K42 Configuration Bit Settings
// CONFIG1L
#pragma config FEXTOSC = OFF			// External Oscillator Selection (Oscillator not enabled)
#pragma config RSTOSC = HFINTOSC_64MHZ	// Reset Oscillator Selection (HFINTOSC with HFFRQ = 64 MHz and CDIV = 1:1)

// CONFIG1H
#pragma config CLKOUTEN = OFF			// Clock out Enable bit (CLKOUT function is disabled)
#pragma config PR1WAY = ON				// PRLOCKED One-Way Set Enable bit (PRLOCK bit can be cleared and set only once)
#pragma config CSWEN = ON				// Clock Switch Enable bit (Writing to NOSC and NDIV is allowed)
#pragma config FCMEN = OFF				// Fail-Safe Clock Monitor Enable bit (Fail-Safe Clock Monitor disabled)

// CONFIG2L
#pragma config MCLRE = EXTMCLR			// MCLR Enable bit (If LVP = 0, MCLR pin is MCLR; If LVP = 1, RE3 pin function is MCLR )
#pragma config PWRTS = PWRT_16			// Power-up timer selection bits (PWRT set at 16ms)
#pragma config MVECEN = ON				// Multi-vector enable bit (Multi-vector enabled, Vector table used for interrupts)
#pragma config IVT1WAY = ON				// IVTLOCK bit One-way set enable bit (IVTLOCK bit can be cleared and set only once)
#pragma config LPBOREN = OFF			// Low Power BOR Enable bit (ULPBOR disabled)
#pragma config BOREN = ON				// Brown-out Reset Enable bits (Brown-out Reset enabled)

// CONFIG2H
#pragma config BORV = VBOR_2P45			// Brown-out Reset Voltage Selection bits (Brown-out Reset Voltage (VBOR) set to 2.45V)
#pragma config ZCD = OFF				// ZCD Disable bit (ZCD disabled. ZCD can be enabled by setting the ZCDSEN bit of ZCDCON)
#pragma config PPS1WAY = OFF			// PPSLOCK bit One-Way Set Enable bit (PPSLOCK bit can be cleared and set only once; PPS registers remain locked after one clear/set cycle)
#pragma config STVREN = ON				// Stack Full/Underflow Reset Enable bit (Stack full/underflow will cause Reset)
#pragma config DEBUG = OFF				// Debugger Enable bit (Background debugger disabled)
#pragma config XINST = OFF				// Extended Instruction Set Enable bit (Extended Instruction Set and Indexed Addressing Mode disabled)

// CONFIG3L
#pragma config WDTCPS = WDTCPS_31		// WDT Period selection bits (Divider ratio 1:65536; software control of WDTPS)
#pragma config WDTE = OFF				// WDT operating mode (WDT Disabled; SWDTEN is ignored)

// CONFIG3H
#pragma config WDTCWS = WDTCWS_7		// WDT Window Select bits (window always open (100%); software control; keyed access not required)
#pragma config WDTCCS = SC				// WDT input clock selector (Software Control)

// CONFIG4L
#pragma config BBSIZE = BBSIZE_512		// Boot Block Size selection bits (Boot Block size is 512 words)
#pragma config BBEN = OFF				// Boot Block enable bit (Boot block disabled)
#pragma config SAFEN = OFF				// Storage Area Flash enable bit (SAF disabled)
#pragma config WRTAPP = OFF				// Application Block write protection bit (Application Block not write protected)

// CONFIG4H
#pragma config WRTB = OFF				// Configuration Register Write Protection bit (Configuration registers (300000-30000Bh) not write-protected)
#pragma config WRTC = OFF				// Boot Block Write Protection bit (Boot Block (000000-0007FFh) not write-protected)
#pragma config WRTD = OFF				// Data EEPROM Write Protection bit (Data EEPROM not write-protected)
#pragma config WRTSAF = OFF				// SAF Write protection bit (SAF not Write Protected)
#pragma config LVP = OFF				// Low Voltage Programming Enable bit (HV on MCLR/VPP must be used for programming)

// CONFIG5L
#pragma config CP = OFF					// PFM and Data EEPROM Code Protection bit (PFM and Data EEPROM code protection disabled)

// CONFIG5H

// #pragma config statements should precede project file includes.
// Use project enums instead of #define for ON and OFF.

#include <xc.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <pic18f24k42.h>
#include "main.h"
#include "ILI9341.h"

void TextDemo(void);
void RectangleDemo(void);
void CircleDemo(void);

void main()
{
	LATA = 0;
	LATB = 0;
	LATC = 0;
	ANSELA = 0;
	ANSELB = 0;
	ANSELC = 0;
    TRISA = 0b00000000;
    TRISB = 0b00000000;
    TRISC = 0b00000000;
	SLRCONC = 0b00000111; // Slew rate needs to be unlimited for at least the SDO & SCK pins to operate at max SPI speed.
// Configure SPI port
	RC6PPS = 0b00011111; // RC6 = SDO
	RC7PPS = 0b00011110; // RC7 = SCK
	SPI1CLK = 0b00000001; // Use 64MHz HFINTOSC for SPI clock source
	SPI1BAUD = 0x00; // = 32MHz SPI clock
	SPI1CON0 = 0b00000011; // SPI master, SPI1TWIDTH setting applies to every byte
	SPI1CON1 = 0b11000100; // SDI input sampled at end of data output time, output data is changed on SCK active to idle transition, SCK idles low, SDO/SDI active high.
	SPI1CON2 = 0b00000010; // SPI TX only
// Enable SPI port and set up display
	SPI1CON0bits.EN = 1;
	LCD_Init();
	LCD_Clear();

	uint8_t next_demo = 0;
	while (1)
	{
		switch (next_demo)
		{
			case 0:
				TextDemo();
				break;
			case 1:
				RectangleDemo();
				break;
			case 2:
				CircleDemo();
				break;
			default:
				break;
		}
		if (++next_demo == 3)
			next_demo = 0;
		__delay_ms(3000);
		LCD_Blank();
	}
}

void TextDemo()
{
	POINT_COLOR = WHITE;
	LCD_DrawString(219,0, "Lorem ipsum dolor sit amet", 2);
	LCD_DrawString(205,0,
				"consectetur adipiscing elit, sed do eiusmod tempor\n"
				"incididunt ut labore et dolore magna aliqua. Ut enim\n"
				"ad minim veniam, quis nostrud exercitation ullamco\n"
				"laboris nisi ut aliquip ex ea commodo consequat. Duis\n"
				"aute irure dolor in reprehenderit in voluptate velit\n"
				"esse cillum dolore eu fugiat nulla pariatur.\n"
				"Excepteur sint occaecat cupidatat non proident, sunt\n"
				"in culpa qui officia deserunt mollit anim id est\n"
				"laborum.", 1);
	POINT_COLOR = RED;
	LCD_DrawString(115,0, "RED", 2);
	POINT_COLOR = GREEN;
	LCD_DrawString(99,0, "GREEN", 2);
	POINT_COLOR = BLUE;
	LCD_DrawString(83,0, "BLUE", 2);
	POINT_COLOR = LGRAY;
	LCD_DrawString(50,0, "2021-02-01 12:34", 2);
	POINT_COLOR = YELLOW;
	LINE_START = 210;
	LCD_DrawString(115,210, "ABCDEFGHIJKLMNOPQRSTUVWXYZ", 3);
	LINE_START = 0;
	LCD_DrawString(0,0, "0123456789", 3);
}

void RectangleDemo()
{
	POINT_COLOR = WHITE;
	LINE_START = 10;
	LCD_DrawString(209,10, "Rectangles + Fill\n+ Lines", 2);
	LINE_START = 0;
	for (uint8_t x = 0; x < 5; x++)
		LCD_DrawRectangle(0+x, 0+x, 239-x, (uint16_t) 319-x);

	POINT_COLOR = BLUE;
	LCD_DrawRectangle(14,54,49,89);
	POINT_COLOR = GREEN;
	LCD_DrawRectangle(156,195,191,230);
	POINT_COLOR = RED;
	LCD_DrawRectangle(84,124,119,159);

	LCD_Fill(120,159,155,194, RED);
	LCD_Fill(49,89,84,124, GREEN);
	LCD_Fill(192,231,227,266, BLUE);

	POINT_COLOR = YELLOW;
	LCD_DrawLine(49,54,227,231);
	LCD_DrawLine(14,89,192,266);
	LCD_DrawLine(48,55,15,88);
	LCD_DrawLine(226,232,193,265);
}

void CircleDemo()
{
	POINT_COLOR = WHITE;
	LCD_DrawString(209,10, "Circles", 2);
	LCD_DrawCircle(119,108,50);
	POINT_COLOR = RED;
	LCD_DrawCircle(119,133,50);
	POINT_COLOR = GREEN;
	LCD_DrawCircle(119,159,50);
	POINT_COLOR = BLUE;
	LCD_DrawCircle(119,185,50);
	POINT_COLOR = YELLOW;
	LCD_DrawCircle(119,210,50);
}