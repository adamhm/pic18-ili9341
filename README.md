# PIC18-ILI9341

Library for using ILI9341-based 240x320 TFT displays with PIC18 microcontrollers.

Includes basic test/demo program for the PIC18F24K42 (or other PIC18F2xK42 microcontrollers).
